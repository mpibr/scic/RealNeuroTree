#include <Adafruit_NeoPixel.h>

// declare functions
void setup(void);
void loop(void);
void InitialiseClusters(void);
void InitialisePixels(void);
void InitialiseEcho(void);
void PingEcho(void);
void InterruptEcho(void);

// declare pins
const int pin_Pixels = 6;
const int pin_Vcc = 11;
const int pin_Trigger = 12;
const int pin_Echo = 2;

// declare dimensions
const int size_Pixels = 100;
const int size_Clusters = 7;
const int size_Spike = 200;

// spike model
const int spike[size_Spike] = {0,1,3,4,4,21,39,62,84,105,122,136,147,156,163,173,181,190,199,207,216,224,232,240,244,249,252,254,255,255,254,251,248,246,242,239,236,232,229,225,222,218,214,210,206,202,198,194,190,186,182,178,174,171,167,163,160,156,152,149,145,142,138,135,131,127,123,120,117,114,111,108,105,102,99,97,95,92,90,88,86,84,81,79,77,75,73,71,69,68,66,64,63,62,60,59,58,57,55,54,53,52,51,50,48,47,45,44,43,42,40,39,37,37,36,34,33,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,13,12,11,11,10,9,9,8,8,8,8,7,7,6,6,6,6,5,5,5,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,0};

// containers
typedef struct _Pixel
{
    int indexSpike;
    int indexCluster;
    int state;
} Pixel;

typedef struct _Cluster
{
    int colorRed;
    int colorGreen;
    int colorBlue;
} Cluster;

Pixel list_Pixels[size_Pixels];
Cluster list_Clusters[size_Clusters];

// events timing in ms
const unsigned long ticEventLow = 400;
const unsigned long ticEventHigh = 2000;
const unsigned long ticDelay = 2;
unsigned long ticEventLast = 0;
unsigned long tic;

// events variables
const int STATE_NULL = 0;
const int STATE_SPIKE = 1;
const int STATE_WAVE = 2;

// wave variables
int indexClusterLast = 0;
int wave_Front = -1;
int wave_Pixels = 3;
const long wave_ProbabilityDefault = 5;
long wave_Probability = wave_ProbabilityDefault;

// echo variables
volatile float pingTime;
volatile float targetDistance;
const float targetDistanceLow = 0.1; // [m]
const float targetDistanceHigh = 2.0; // [m]
const float speedOfSound  = 343.0; // in [m/s] @ temperature of 21deg Celsius
long ticEcho;

// set colors
int colorRed = 0;
int colorGreen = 0;
int colorBlue = 0;

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(size_Pixels, pin_Pixels, NEO_RGB + NEO_KHZ800);

void setup()
{
    // add Serial
    Serial.begin(9600);
  
    // reset random seed
    randomSeed(analogRead(0));
    
    InitialiseClusters();
    InitialisePixels();
    InitialiseEcho();


    // initialise timing
    tic = millis();
    
    // This initializes the NeoPixel library
    pixels.begin(); 
    
}

void loop()
{
    float colorWeight;
    
    // reset colors
    colorRed = 0;
    colorGreen = 0;
    colorBlue = 0;
    
    // event time?
    if ((millis() - tic) > ticEventLast)
    {
      
        // update event properties
        ticEventLast = random(ticEventLow, ticEventHigh + 1);
        tic = millis();
        indexClusterLast = random(0, size_Clusters + 1);
        
        // spike or wave
        if(random(0,100) <  wave_Probability)
        {
            wave_Front += wave_Pixels; // trigger wave
            //Serial.println("Event: Wave!");
        }
        else
        {
            //Serial.print("Event: Spike ");
            //Serial.print(indexClusterLast);
            //Serial.println();
            
        }
        
    }
    
    // increase wave front
    if(wave_Front > -1)
    {
        wave_Front += wave_Pixels;
    }
    
    // loop over the pixels
    for(int p = 0; p < size_Pixels; p++)
    {
        
        // set spike state
        if ((list_Pixels[p].state == STATE_NULL) &&
            (list_Pixels[p].indexCluster == indexClusterLast))
        {
            list_Pixels[p].state = STATE_SPIKE;
        }
        
        // set wave state
        if ((p < wave_Front) && (list_Pixels[p].state != STATE_WAVE))
        {
            list_Pixels[p].indexSpike = -1;
            list_Pixels[p].state = STATE_WAVE;
        }
        
        
        // state spike
        if (list_Pixels[p].state == STATE_SPIKE)
        {
            // increment index spike
            list_Pixels[p].indexSpike++;
            
            // adjust colors
            if (list_Pixels[p].indexSpike < size_Spike)
            {
                colorWeight = (float) spike[list_Pixels[p].indexSpike] / 255.0;
                colorRed = (int)(list_Clusters[list_Pixels[p].indexCluster].colorRed * colorWeight);
                colorGreen = (int)(list_Clusters[list_Pixels[p].indexCluster].colorGreen * colorWeight);
                colorBlue = (int)(list_Clusters[list_Pixels[p].indexCluster].colorBlue * colorWeight);

                // Arduino set colors per pixel code
                pixels.setPixelColor(p, pixels.Color(colorRed, colorGreen,colorBlue));
                
            }
            else
            {
                list_Pixels[p].indexSpike = -1;
                list_Pixels[p].state = STATE_NULL;
            }

        }
        
        
        // state wave
        if (list_Pixels[p].state == STATE_WAVE)
        {
            // increment index spike
            list_Pixels[p].indexSpike++;
            
            // adjust colors
            if (list_Pixels[p].indexSpike < size_Spike)
            {
                colorWeight = spike[list_Pixels[p].indexSpike] / 255.0;
                colorRed = (int)(255 * colorWeight);
                colorGreen = (int)(255 * colorWeight);
                colorBlue = (int)(255 * colorWeight);
                
                // Arduino set colors per pixel code
                pixels.setPixelColor(p, pixels.Color(colorRed, colorGreen,colorBlue));
            }
            else
            {
                list_Pixels[p].indexSpike = -1;
                list_Pixels[p].state = STATE_NULL;
            }
        }

        
        
        

        //Serial.print(list_Pixels[p].indexCluster);
    }
    //Serial.println();
    
    // wave reached last element
    if(wave_Front > size_Pixels)
    {
        wave_Front = -1;
    }

    // render new pixels colors
    pixels.show();

    // update targetDistance
    PingEcho();
    
    // link wave probability to target distance
    if ((targetDistanceLow <= targetDistance) && (targetDistance <= targetDistanceHigh))
    {
       float targetWeight = (float) (targetDistance - targetDistanceLow)/(targetDistanceHigh - targetDistanceLow);
       wave_Probability = (long)((1 - targetWeight)*100);
      //Serial.print(wave_Probability);
      //Serial.println();
       
    }
    else
    {
        wave_Probability = wave_ProbabilityDefault;
    }
    
    

    // fragment
    delay(ticDelay);        
    
}


/*
 * INITIALISECLUSTERS
 *  add clusters specific colors
 */
void InitialiseClusters()
{
    list_Clusters[0] = (Cluster){ .colorRed = 0, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[1] = (Cluster){ .colorRed = 0, .colorGreen = 255, .colorBlue = 0};
    list_Clusters[2] = (Cluster){ .colorRed = 170, .colorGreen = 0, .colorBlue = 170};
    list_Clusters[3] = (Cluster){ .colorRed = 127, .colorGreen = 255, .colorBlue = 127};
    list_Clusters[4] = (Cluster){ .colorRed = 127, .colorGreen = 0, .colorBlue = 127};
    list_Clusters[5] = (Cluster){ .colorRed = 255, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[6] = (Cluster){ .colorRed = 75, .colorGreen = 0, .colorBlue = 130};
}


/*
 * INITIALISEPIXELS
 *  set pixels default value
 *  define random pixel clusters
 */
void InitialisePixels()
{
    for(int p = 0; p < size_Pixels; p++)
    {
        list_Pixels[p].indexCluster = random(0, size_Clusters + 1);
        list_Pixels[p].indexSpike = -1;
        list_Pixels[p].state = STATE_NULL;
    }
}


/*
 * INITIALISEECHO
 *  set pins I/O direction
 *  power echo module
 */
 void InitialiseEcho()
 {
     pinMode(pin_Vcc, OUTPUT);
     pinMode(pin_Trigger, OUTPUT);
     pinMode(pin_Echo, INPUT);

     digitalWrite(pin_Vcc, HIGH);

     attachInterrupt(digitalPinToInterrupt(pin_Echo), InterruptEcho, HIGH);
 }


/*
 * PINGECHO
 *  send echo pulse
 *  calculate distance
 */
void PingEcho()
{
  // generate echo pulse
  digitalWrite(pin_Trigger, LOW); 
  delayMicroseconds(100);
  digitalWrite(pin_Trigger, HIGH);
  delayMicroseconds(15);
  digitalWrite(pin_Trigger, LOW);
  
  ticEcho = micros();

}

void InterruptEcho()
{
  // read in echo pulse
  pingTime = micros() - ticEcho; // [microseconds]

  //pingTime = pulseIn(pin_Echo, HIGH, 4000); // [microseconds]
  pingTime = pingTime / 1000000; // [seconds]
  targetDistance = speedOfSound * pingTime; // [meters]
  targetDistance = targetDistance / 2; // ping traveled both directions
  
}

