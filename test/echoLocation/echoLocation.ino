const int pin_Vcc = 11;
const int pin_Trigger = 12;
const int pin_Echo = 13;

float pingTime;
float targetDistance;
const float speedOfSound = 343.0; // in [m/s] @ temperature of 21deg Celsius

void setup()
{
    Serial.begin(9600);
    pinMode(pin_Vcc, OUTPUT);
    pinMode(pin_Trigger, OUTPUT);
    pinMode(pin_Echo, INPUT);

    digitalWrite(pin_Vcc, HIGH);

}

void loop()
{
  // generate echo pulse
  digitalWrite(pin_Trigger, LOW); 
  delayMicroseconds(2000);
  digitalWrite(pin_Trigger, HIGH);
  delayMicroseconds(15);
  digitalWrite(pin_Trigger, LOW);
  delayMicroseconds(10);

  // read in echo pulse
  pingTime = pulseIn(pin_Echo, HIGH); // [microseconds]
  pingTime = pingTime / 1000000; // [seconds]
  targetDistance = speedOfSound * pingTime; // [meters]
  targetDistance = targetDistance / 2; // ping traveled both directions

  // report distance
  Serial.print("D = ");
  Serial.print(targetDistance);
  Serial.println(" meters");

  // discretize
  delay(100); // [milliseconds]
}

