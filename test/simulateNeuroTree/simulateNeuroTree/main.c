//
//  main.c
//  simulateNeuroTree
//
//  Created by Georgi Tushev on 06.12.17.
//  Copyright © 2017 Georgi Tushev. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// declare functions
void setup(void);
void loop(void);
void InitialiseClusters(void);
void InitialisePixels(void);

// declare global variables
const int size_Pixels = 50;
const int size_Clusters = 7;
const int size_Spike = 200;

const float spike[size_Spike] = {0.0,1.21,2.55,3.64,4.45,20.53,39.11,61.71,84.28,104.8400,121.8700,135.9,146.7400,155.5100,163.2300,172.5,181.4500,190.1,198.8600,207.3900,215.7300,224.0100,232.1500,239.6200,244.4200,248.8800,251.9500,254.3800,255,254.7500,253.7200,251.3100,248.3600,245.6500,242.0500,239.0100,235.7600,232.4700,228.7700,225.2800,222.2600,218.2300,214.1,210.3,206.4500,202.4800,198.4300,194.2700,190.2400,186.1,182.1700,177.9200,174.4200,170.9,167.2300,163.4400,159.7100,155.9900,152.4500,148.7300,145.1500,141.7400,137.8900,134.9200,130.9400,127.2600,123.3500,120.2300,117.3900,114.0600,111.4400,108.4800,105.2500,102.0500,99.17,96.8,94.62,92.46,90.04,87.89,85.8,83.55,81.32,79.18,77,74.88,72.92,71.01,69.41,67.79,66.02,64.46,62.79,61.75,60.32,59.13,57.73,56.58,55.17,53.9,52.85,51.93,50.62,49.59,48.31,46.86,45.39,44.14,43.01,41.59,40.46,39.31,37.46,36.6,35.6,34.06,32.66,31.25,29.83,28.81,27.99,26.96,25.97,25,24.01,23.22,22.07,21.08,20.03,19.02,17.73,17.01,15.82,15.12,14.29,13.47,12.93,11.8,11.17,10.52,9.67,9.48,8.85,8.41,8.22,7.95,7.66,7.47,7.01,6.32,5.96,5.67,5.51,5.24,4.74,4.61,4.30,4.30,4.34,4.22,3.92,3.92,3.46,3.46,3.30,3.11,3.07,3.19,3.05,3.21,2.89,2.85,2.83,2.80,2.80,2.80,2.80,2.80,2.80,2.80,2.80,2.80,2.80,2.0,2.0,2.0,2.0,2.0,2.0,2.0,1.50,1.50,1.50,1.50,1.50,1.0,1.0,1.0,1.0,0.0};

typedef struct _Pixel
{
    int indexSpike;
    int indexCluster;
    int state;
} Pixel;

typedef struct _Cluster
{
    int colorRed;
    int colorGreen;
    int colorBlue;
} Cluster;

Pixel list_Pixels[size_Pixels];
Cluster list_Clusters[size_Clusters];

// events timing
struct timespec tic, toc;
uint64_t ticEventLow = 200;
uint64_t ticEventHigh = 1000;
uint64_t ticEventLast = 0;

// events variables
const int STATE_NULL = 0;
const int STATE_SPIKE = 1;
const int STATE_WAVE = 2;

int indexClusterLast = 0;
int wave_Front = -1;
int wave_Pixels = 5;
double wave_Probability = 0.1;



int main(int argc, const char * argv[])
{
    int loopCount = 0;
    
    // evoke setup
    setup();
    
    // evoke loop
    while(loopCount < 100000)
    {
        loop();
        loopCount++;
    }
    
    return 0;
}


void setup()
{
    // reset random seed
    srand((unsigned)time(0));
    
    InitialiseClusters();
    InitialisePixels();
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &tic);
    
    
}

void loop()
{
    float colorWeight;
    int colorRed;
    int colorGreen;
    int colorBlue;
    
    // get time since last event
    clock_gettime(CLOCK_MONOTONIC_RAW, &toc);
    uint64_t delta_us = (toc.tv_sec - tic.tv_sec) * 1000000 + (toc.tv_nsec - tic.tv_nsec) / 1000;
    
    // event time?
    if (delta_us > ticEventLast)
    {
        // update event properties
        ticEventLast = ticEventLow + rand() / (RAND_MAX / (ticEventHigh - ticEventLow + 1) + 1);
        clock_gettime(CLOCK_MONOTONIC_RAW, &tic);
        indexClusterLast = rand() / (RAND_MAX / size_Clusters + 1);
        
        // spike or wave
        if(rand() <  wave_Probability * ((double)RAND_MAX + 1.0))
        {
            printf("Wave: %llu\n", delta_us);
            wave_Front += wave_Pixels; // trigger wave
        }
        else
        {
            printf("Spike: %llu\n", delta_us);
        }
        
        
    }
    
    // increase wave front
    if(wave_Front > -1)
    {
        wave_Front += wave_Pixels;
    }
    
    // loop over the pixels
    for(int p = 0; p < size_Pixels; p++)
    {
        
        // set spike state
        if ((list_Pixels[p].state == STATE_NULL) &&
            (list_Pixels[p].indexCluster == indexClusterLast))
        {
            list_Pixels[p].state = STATE_SPIKE;
        }
        
        // set wave state
        if ((p < wave_Front) && (list_Pixels[p].state != STATE_WAVE))
        {
            list_Pixels[p].indexSpike = -1;
            list_Pixels[p].state = STATE_WAVE;
        }
        
        // increment index spike curve
        list_Pixels[p].indexSpike++;
        
        // adjust colors
        if (list_Pixels[p].indexSpike < size_Spike)
        {
            colorWeight = spike[list_Pixels[p].indexSpike] / 255.0;
            
            // spike state
            if (list_Pixels[p].state == STATE_SPIKE)
            {
                colorRed = (int)(list_Clusters[list_Pixels[p].indexCluster].colorRed * colorWeight);
                colorGreen = (int)(list_Clusters[list_Pixels[p].indexCluster].colorGreen * colorWeight);
                colorBlue = (int)(list_Clusters[list_Pixels[p].indexCluster].colorBlue * colorWeight);
            }
            
            // wave state
            if (list_Pixels[p].state == STATE_WAVE)
            {
                colorRed = (int)(255 * colorWeight);
                colorGreen = (int)(255 * colorWeight);
                colorBlue = (int)(255 * colorWeight);
            }
            
            // Arduino set colors per pixel code
            //
            
        }
        else // finish spike curve
        {
            list_Pixels[p].indexSpike = -1;
            list_Pixels[p].state = STATE_NULL;
        }
        
        
        printf("%d",list_Pixels[p].state);
    }
    printf("\n");
    
    // wave reached last element
    if(wave_Front > size_Pixels)
    {
        wave_Front = -1;
    }
}

/*
 * INITIALISECLUSTERS
 *  add clusters specific colors
 */
void InitialiseClusters()
{
    list_Clusters[0] = (Cluster){ .colorRed = 0, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[1] = (Cluster){ .colorRed = 0, .colorGreen = 255, .colorBlue = 0};
    list_Clusters[2] = (Cluster){ .colorRed = 170, .colorGreen = 0, .colorBlue = 170};
    list_Clusters[3] = (Cluster){ .colorRed = 127, .colorGreen = 255, .colorBlue = 127};
    list_Clusters[4] = (Cluster){ .colorRed = 127, .colorGreen = 0, .colorBlue = 127};
    list_Clusters[5] = (Cluster){ .colorRed = 255, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[6] = (Cluster){ .colorRed = 255, .colorGreen = 127, .colorBlue = 127};
}

/*
 * INITIALISEPIXELS
 *  set pixels default value
 *  define random pixel clusters
 */
void InitialisePixels()
{
    int indexCluster;
    
    for(int p = 0; p < size_Pixels; p++)
    {
        indexCluster = rand() / (RAND_MAX / size_Clusters + 1);
        list_Pixels[p].indexCluster = indexCluster;
        list_Pixels[p].indexSpike = -1;
        list_Pixels[p].state = STATE_NULL;
    }
}
