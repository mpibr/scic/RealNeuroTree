#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      50

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

void initializeClusters(void);
void initializePixels(void);

// declare global variables
const int size_Pixels = 50;
const int size_Clusters = 7;
const int size_Spike = 200;

const int spike[size_Spike] = {0,1,3,4,4,21,39,62,84,105,122,136,147,156,163,173,181,190,199,207,216,224,232,240,244,249,252,254,255,255,254,251,248,246,242,239,236,232,229,225,222,218,214,210,206,202,198,194,190,186,182,178,174,171,167,163,160,156,152,149,145,142,138,135,131,127,123,120,117,114,111,108,105,102,99,97,95,92,90,88,86,84,81,79,77,75,73,71,69,68,66,64,63,62,60,59,58,57,55,54,53,52,51,50,48,47,45,44,43,42,40,39,37,37,36,34,33,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,13,12,11,11,10,9,9,8,8,8,8,7,7,6,6,6,6,5,5,5,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,0};

typedef struct _Pixel
{
    int indexSpike;
    int indexCluster;
    int state;
} Pixel;

typedef struct _Cluster
{
    int colorRed;
    int colorGreen;
    int colorBlue;
} Cluster;

Pixel list_Pixels[size_Pixels];
Cluster list_Clusters[size_Clusters];


// events timing
int delayVal = 2; //in ms
unsigned int tic = 0;
unsigned int toc = 0;
unsigned int ticEventLow = 200; //in ms
unsigned int ticEventHigh = 1000;
unsigned int ticEventLast = 0; 

// events variables
const int STATE_NULL = 0;
const int STATE_SPIKE = 1;
const int STATE_WAVE = 2;

int indexClusterLast = 0;
int wave_Front = -1;
int wave_Pixels = 5;
double wave_Probability = 0.1;

float colorWeight;
int colorRed;
int colorGreen;
int colorBlue;

//const int RAND_MAX = 32767;


void setup() {
  randomSeed(analogRead(0));  
  initializeClusters();
  initializePixels();
  tic = micros();
  
  Serial.begin(9600);      // open the serial port at 250000 bps:
  pixels.begin(); // This initializes the NeoPixel library.
}

void loop() {
  // get time since last event
    toc = millis();
    unsigned int delta_us = toc-tic;
    
    // event time?
    if (delta_us > ticEventLast)
    {
        // update event properties
        ticEventLast = ticEventLow + random(0, RAND_MAX) / (RAND_MAX / (ticEventHigh - ticEventLow + 1) + 1);
        tic = millis();
        indexClusterLast = random(0, RAND_MAX) / (RAND_MAX / size_Clusters + 1);
        
        // spike or wave
        if(random(0, RAND_MAX) <  wave_Probability * ((double)RAND_MAX + 1.0))
        {
            Serial.print("Wave: ");
            Serial.print(delta_us);
            Serial.println();
            wave_Front += wave_Pixels; // trigger wave
        }
        else
        {
            Serial.print("Spike: ");
            Serial.print(delta_us);
            Serial.println();
        }
        
        
    }
    
    // increase wave front
    if(wave_Front > -1)
    {
        wave_Front += wave_Pixels;
    }
    
    // loop over the pixels
    for(int p = 0; p < size_Pixels; p++)
    {
        
        // set spike state
        if ((list_Pixels[p].state == STATE_NULL) &&
            (list_Pixels[p].indexCluster == indexClusterLast))
        {
            list_Pixels[p].state = STATE_SPIKE;
        }
        
        // set wave state
        if ((p < wave_Front) && (list_Pixels[p].state != STATE_WAVE))
        {
            list_Pixels[p].indexSpike = -1;
            list_Pixels[p].state = STATE_WAVE;
        }
        
        // increment index spike curve
        list_Pixels[p].indexSpike++;
        
        // adjust colors
        if (list_Pixels[p].indexSpike < size_Spike)
        {
            colorWeight = spike[list_Pixels[p].indexSpike] / 255.0;
            
            // spike state
            if (list_Pixels[p].state == STATE_SPIKE)
            {
                colorRed = (int)(list_Clusters[list_Pixels[p].indexCluster].colorRed * colorWeight);
                colorGreen = (int)(list_Clusters[list_Pixels[p].indexCluster].colorGreen * colorWeight);
                colorBlue = (int)(list_Clusters[list_Pixels[p].indexCluster].colorBlue * colorWeight);
            }
            
            // wave state
            if (list_Pixels[p].state == STATE_WAVE)
            {
                colorRed = (int)(255 * colorWeight);
                colorGreen = (int)(255 * colorWeight);
                colorBlue = (int)(255 * colorWeight);
            }
            
            // Arduino set colors per pixel code
            pixels.setPixelColor(p, pixels.Color(colorRed,colorGreen,colorBlue));
            
        }
        else // finish spike curve
        {
            list_Pixels[p].indexSpike = -1;
            list_Pixels[p].state = STATE_NULL;
        }
        
        
        //printf("%d",list_Pixels[p].state);
    }
    //printf("\n");
    
    // wave reached last element
    if(wave_Front > size_Pixels)
    {
        wave_Front = -1;
    }
     
 
  pixels.show(); //show the new state
  
  delay(delayVal);
}


/*
 * INITIALIZECLUSTERS
 *  add clusters specific colors
 */
void initializeClusters()
{
    list_Clusters[0] = (Cluster){ .colorRed = 0, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[1] = (Cluster){ .colorRed = 0, .colorGreen = 255, .colorBlue = 0};
    list_Clusters[2] = (Cluster){ .colorRed = 170, .colorGreen = 0, .colorBlue = 170};
    list_Clusters[3] = (Cluster){ .colorRed = 127, .colorGreen = 255, .colorBlue = 127};
    list_Clusters[4] = (Cluster){ .colorRed = 127, .colorGreen = 0, .colorBlue = 127};
    list_Clusters[5] = (Cluster){ .colorRed = 255, .colorGreen = 0, .colorBlue = 255};
    list_Clusters[6] = (Cluster){ .colorRed = 255, .colorGreen = 127, .colorBlue = 127};
}

/*
 * INITIALISEPIXELS
 *  set pixels default value
 *  define random pixel clusters
 */
void initializePixels()
{
    int indexCluster;
    
    for(int p = 0; p < size_Pixels; p++)
    {
        indexCluster = random(0, RAND_MAX) / (RAND_MAX / size_Clusters + 1);
        list_Pixels[p].indexCluster = indexCluster;
        list_Pixels[p].indexSpike = -1;
        list_Pixels[p].state = STATE_NULL;
    }
}

